var searchData=
[
  ['parameter',['Parameter',['../class_material_design_themes_1_1_wpf_1_1_dialog_closing_event_args.html#aad113e883e078752b25e84e3197c10b3',1,'MaterialDesignThemes::Wpf::DialogClosingEventArgs']]],
  ['placementmode',['PlacementMode',['../class_material_design_themes_1_1_wpf_1_1_popup_box.html#a8fac833fb72a96c6265deeb301da40d4',1,'MaterialDesignThemes::Wpf::PopupBox']]],
  ['playerships',['PlayerShips',['../class_battleship_game_1_1_player.html#ae16082caa1ec5927dcc416872ac45ca3',1,'BattleshipGame::Player']]],
  ['popupcontent',['PopupContent',['../class_material_design_themes_1_1_wpf_1_1_popup_box.html#a3a864de07bba3d68f25a2fef02d046d6',1,'MaterialDesignThemes::Wpf::PopupBox']]],
  ['popupcontenttemplate',['PopupContentTemplate',['../class_material_design_themes_1_1_wpf_1_1_popup_box.html#ad1de44df3b42d9fdaed68985a956ccb7',1,'MaterialDesignThemes::Wpf::PopupBox']]],
  ['popupmode',['PopupMode',['../class_material_design_themes_1_1_wpf_1_1_popup_box.html#aafb69076f3bc84fe420cc6ce2dab2d43',1,'MaterialDesignThemes::Wpf::PopupBox']]],
  ['positions',['Positions',['../class_battleship_game_1_1_ships.html#a66aa92cb97b149ceb33d4a2b8ae08529',1,'BattleshipGame::Ships']]],
  ['preparation',['Preparation',['../class_battleship_game_1_1_view_model.html#a59a5ec305577d6fadd507d08b5029af3',1,'BattleshipGame::ViewModel']]]
];
