var searchData=
[
  ['badged',['Badged',['../class_material_design_themes_1_1_wpf_1_1_badged.html',1,'MaterialDesignThemes::Wpf']]],
  ['battlegroundwindow',['BattlegroundWindow',['../class_battleship_game_1_1_battleground_window.html',1,'BattleshipGame.BattlegroundWindow'],['../class_battleship_game_1_1_battleground_window.html#ac00b1734bd0bb4c59aff6e4242b34ce8',1,'BattleshipGame.BattlegroundWindow.BattlegroundWindow()']]],
  ['battleship',['BattleShip',['../class_battleship_game_1_1_battle_ship.html',1,'BattleshipGame.BattleShip'],['../class_battleship_game_1_1_battle_ship.html#a05fd59c6183e599fc165ee2cfb7d1d70',1,'BattleshipGame.BattleShip.BattleShip()']]],
  ['battleshipgame',['BattleshipGame',['../namespace_battleship_game.html',1,'']]],
  ['booleantovisibilityconverter',['BooleanToVisibilityConverter',['../class_material_design_themes_1_1_wpf_1_1_converters_1_1_boolean_to_visibility_converter.html',1,'MaterialDesignThemes::Wpf::Converters']]],
  ['bot',['Bot',['../class_battleship_game_1_1_bot.html',1,'BattleshipGame.Bot'],['../class_battleship_game_1_1_bot.html#afeb12ce1f9d2851c998993611cbeb266',1,'BattleshipGame.Bot.Bot()']]],
  ['bottomandaligncentres',['BottomAndAlignCentres',['../namespace_material_design_themes_1_1_wpf.html#aa5ff74ea870d218ca527b0313b4f89a7a3074a9fcab8ac0d41bdc1905fb67509b',1,'MaterialDesignThemes::Wpf']]],
  ['bottomandalignleftedges',['BottomAndAlignLeftEdges',['../namespace_material_design_themes_1_1_wpf.html#aa5ff74ea870d218ca527b0313b4f89a7a74ae26bd3433eee5bbb4fa8c68f697d2',1,'MaterialDesignThemes::Wpf']]],
  ['bottomandalignrightedges',['BottomAndAlignRightEdges',['../namespace_material_design_themes_1_1_wpf.html#aa5ff74ea870d218ca527b0313b4f89a7a4a0258bdb4a0c60876e9ecf41ab97ac1',1,'MaterialDesignThemes::Wpf']]],
  ['brushroundconverter',['BrushRoundConverter',['../class_material_design_themes_1_1_wpf_1_1_converters_1_1_brush_round_converter.html',1,'MaterialDesignThemes::Wpf::Converters']]],
  ['brushtoradialgradientbrushconverter',['BrushToRadialGradientBrushConverter',['../class_material_design_themes_1_1_wpf_1_1_converters_1_1_brush_to_radial_gradient_brush_converter.html',1,'MaterialDesignThemes::Wpf::Converters']]],
  ['beadandó',['Beadandó',['../md__c_1__users__norbert__documents__visual__studio_2015__projects_battleshipgit__r_e_a_d_m_e.html',1,'']]],
  ['properties',['Properties',['../namespace_battleship_game_1_1_properties.html',1,'BattleshipGame']]]
];
