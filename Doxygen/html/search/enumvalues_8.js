var searchData=
[
  ['right',['Right',['../namespace_battleship_game.html#aa4e9adf065f1959561a74e1ad61e6f64a92b09c7c48c520c3c55e497875da437c',1,'BattleshipGame']]],
  ['rightandalignbottomedges',['RightAndAlignBottomEdges',['../namespace_material_design_themes_1_1_wpf.html#aa5ff74ea870d218ca527b0313b4f89a7a36b7fb6993d390e89e0ab47de82d9612',1,'MaterialDesignThemes::Wpf']]],
  ['rightandalignmiddles',['RightAndAlignMiddles',['../namespace_material_design_themes_1_1_wpf.html#aa5ff74ea870d218ca527b0313b4f89a7a90b045ee0ff45d5be100a992781be2d0',1,'MaterialDesignThemes::Wpf']]],
  ['rightandaligntopedges',['RightAndAlignTopEdges',['../namespace_material_design_themes_1_1_wpf.html#aa5ff74ea870d218ca527b0313b4f89a7a2d1447ed26d8c69a4bf0b7a232eb35b0',1,'MaterialDesignThemes::Wpf']]],
  ['rotate',['Rotate',['../namespace_battleship_game.html#aa4e9adf065f1959561a74e1ad61e6f64a8d2de5368588552fbae54044ac5c7b3d',1,'BattleshipGame']]]
];
