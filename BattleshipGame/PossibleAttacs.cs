﻿// <copyright file="PossibleAttacs.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    /// <summary>
    /// Enum with possible attacks.
    /// </summary>
    public enum PossibleAttacks
    {
        /// <summary>
        /// The cell has not been attacked yet.
        /// </summary>
        DidNotAttacked,

        /// <summary>
        /// There is no ship on the cell.
        /// </summary>
        DidNotHit,

        /// <summary>
        /// There is a ship on the cell and have been hit.
        /// </summary>
        Hit,

        /// <summary>
        /// There is a ship on the cell, have been hit and the ship sank.
        /// </summary>
        HitAndSink
    }
}
