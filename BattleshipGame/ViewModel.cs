﻿// <copyright file="ViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// ViewModel class.
    /// </summary>
    public class ViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Size of one field.
        /// </summary>
        public const int Fieldsize = 40;

        /// <summary>
        /// Stores the ViewModel object.
        /// </summary>
        private static ViewModel newViewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// The constructor of the ViewModel class.
        /// </summary>
        private ViewModel()
        {
            this.Preparation = true;
        }

        /// <summary>
        /// Handled if the drawings changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets a value indicating whether the preparation is going on or not.
        /// </summary>
        public bool Preparation { get; set; }

        /// <summary>
        /// Gets or sets the selected ship object.
        /// </summary>
        public Ships SelectedShip { get; set; }

        /// <summary>
        /// Gets or sets the first player object.
        /// </summary>
        public Player FirstPlayer { get; set; }

        /// <summary>
        /// Gets or sets the second player object.
        /// </summary>
        public Player SecondPlayer { get; set; }

        /// <summary>
        /// Gets the first player drawing.
        /// </summary>
        public Drawing FirstPlayerDrawing
        {
            get
            {
                DrawingGroup dg = new DrawingGroup();
                if (this.Preparation && Game.FirstPlayerRound)
                {
                    dg = this.ShipDraw(this.FirstPlayer);
                }
                else if(!this.Preparation)
                {
                    dg = this.DestroyedShips(this.FirstPlayer);
                }

                for (int i = 0; i < this.FirstPlayer.Cells.GetLength(0); i++)
                {
                    for (int j = 0; j < this.FirstPlayer.Cells.GetLength(1); j++)
                    {
                        GeometryDrawing bg = new GeometryDrawing(null, new Pen(Brushes.Gray, 1), new RectangleGeometry(new Rect(j * Fieldsize, i * Fieldsize, Fieldsize, Fieldsize)));
                        switch (this.FirstPlayer.Cells[i, j])
                        {
                            case PossibleAttacks.DidNotAttacked:
                                bg.Brush = Brushes.Transparent;
                                break;
                            case PossibleAttacks.DidNotHit:
                                bg.Brush = Brushes.MintCream;
                                break;
                            case PossibleAttacks.Hit:
                                bg.Brush = Brushes.SkyBlue;
                                break;
                        }

                        dg.Children.Add(bg);
                    }
                }

                return dg;
            }
        }

        /// <summary>
        /// Gets the second player drawing.
        /// </summary>
        public Drawing SecondPlayerDrawing
        {
            get
            {
                DrawingGroup dg = new DrawingGroup();
                if (this.Preparation && !Game.FirstPlayerRound)
                {
                    dg = this.ShipDraw(this.SecondPlayer);
                }
                else if (!this.Preparation)
                {
                    dg = this.DestroyedShips(this.SecondPlayer);
                }

                for (int i = 0; i < this.SecondPlayer.Cells.GetLength(0); i++)
                {
                    for (int j = 0; j < this.SecondPlayer.Cells.GetLength(1); j++)
                    {
                        GeometryDrawing bg = new GeometryDrawing(null, new Pen(Brushes.Gray, 1), new RectangleGeometry(new System.Windows.Rect(j * Fieldsize, i * Fieldsize, Fieldsize, Fieldsize)));
                        switch (this.SecondPlayer.Cells[i, j])
                        {
                            case PossibleAttacks.DidNotAttacked:
                                bg.Brush = Brushes.Transparent;
                                break;
                            case PossibleAttacks.DidNotHit:
                                bg.Brush = Brushes.MintCream;
                                break;
                            case PossibleAttacks.Hit:
                                bg.Brush = Brushes.SkyBlue;
                                break;
                        }

                        dg.Children.Add(bg);
                    }
                }

                return dg;
            }
        }

        /// <summary>
        /// If the ViewModel object is null, then initialize the ViewModel object.
        /// </summary>
        /// <returns> The ViewModel object. </returns>
        public static ViewModel Get()
        {
            if (newViewModel == null)
            {
                newViewModel = new ViewModel();
            }

            return newViewModel;
        }

        /// <summary>
        /// Change the drawing.
        /// </summary>
        public void DrawingChange()
        {
            this.PropertyChanged(this, new PropertyChangedEventArgs("FirstPlayerDrawing"));
            this.PropertyChanged(this, new PropertyChangedEventArgs("SecondPlayerDrawing"));
        }

        /// <summary>
        /// Draw the player's ships.
        /// </summary>
        /// <param name="player"> The selected player. </param>
        /// <returns> DrawingGroup of the draws. </returns>
        private DrawingGroup ShipDraw(Player player)
        {
            DrawingGroup dg = new DrawingGroup();
            for (int i = 0; i < player.PlayerShips.Count; i++)
            {
                GeometryDrawing bg = new GeometryDrawing();
                if (!player.PlayerShips[i].VerticalImage)
                {
                    bg.Brush = new ImageBrush(player.PlayerShips[i].StandardImage);
                    bg.Geometry = new RectangleGeometry(new Rect(player.PlayerShips[i].Positions[0].Y * Fieldsize, player.PlayerShips[i].Positions[0].X * Fieldsize, player.PlayerShips[i].Length * Fieldsize, Fieldsize));
                }
                else
                {
                    bg.Brush = new ImageBrush(player.PlayerShips[i].RotatedImage);
                    bg.Geometry = new RectangleGeometry(new Rect(player.PlayerShips[i].Positions[0].Y * Fieldsize, player.PlayerShips[i].Positions[0].X * Fieldsize, Fieldsize, player.PlayerShips[i].Length * Fieldsize));
                }

                dg.Children.Add(bg);
            }

            return dg;
        }

        private DrawingGroup DestroyedShips(Player player)
        {
            DrawingGroup dg = new DrawingGroup();
            foreach (Ships ship in player.PlayerShips)
            {
                if (ship.Life == 0)
                {
                    GeometryDrawing bg = new GeometryDrawing();
                    if (!ship.VerticalImage)
                    {
                        bg.Brush = new ImageBrush(ship.StandardImage);
                        bg.Geometry = new RectangleGeometry(new Rect(ship.Positions[0].Y * Fieldsize, ship.Positions[0].X * Fieldsize, ship.Length * Fieldsize, Fieldsize));
                    }
                    else
                    {
                        bg.Brush = new ImageBrush(ship.RotatedImage);
                        bg.Geometry = new RectangleGeometry(new Rect(ship.Positions[0].Y * Fieldsize, ship.Positions[0].X * Fieldsize, Fieldsize, ship.Length * Fieldsize));
                    }

                    dg.Children.Add(bg);
                }
            }

            return dg;
        }
    }
}
