﻿// <copyright file="CorvetteShip.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    using System;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// CorvetteShip type Ships subclass.
    /// </summary>
    public class CorvetteShip : Ships
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CorvetteShip"/> class.
        /// The constructor of the CorvetteShip class.
        /// </summary>
        public CorvetteShip()
            : base('C', 3)
        {
            this.StandardImage = new BitmapImage(new Uri("../../images/StandardCorvette.png", UriKind.Relative));
            this.RotatedImage = new BitmapImage(new Uri("../../images/RotatedCorvette.png", UriKind.Relative));
            this.Positions = this.PositionListInit(new Position(3, 0));
        }
    }
}
