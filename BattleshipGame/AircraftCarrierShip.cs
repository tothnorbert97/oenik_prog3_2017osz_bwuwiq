﻿// <copyright file="AircraftCarrierShip.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    using System;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// AircraftCarrierShip type Ships subclass.
    /// </summary>
    public class AircraftCarrierShip : Ships
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AircraftCarrierShip"/> class.
        /// The constructor of the AircraftCarrierShip class.
        /// </summary>
        public AircraftCarrierShip()
            : base('A', 5)
        {
            this.StandardImage = new BitmapImage(new Uri("../../images/StandardAircraftcarrier.png", UriKind.Relative));
            this.RotatedImage = new BitmapImage(new Uri("../../images/RotatedAircraftcarrier.png", UriKind.Relative));
            this.Positions = this.PositionListInit(new Position(0, 0));
        }
    }
}
