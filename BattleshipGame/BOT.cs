﻿// <copyright file="Bot.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Bot type Players subclass.
    /// </summary>
    public class Bot : Player
    {
        /// <summary>
        /// Generate one random integer.
        /// </summary>
        private Random rnd;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bot"/> class.
        /// The constructor of the Bot class.
        /// </summary>
        public Bot()
        {
            this.rnd = new Random();
            this.PlayerShips = this.ShipPositionsINIT(this.PlayerShips);
            this.InitialMatrix = this.InitialMatrixInit(this.PlayerShips);
        }

        /// <summary>
        /// Generates an attackable position.
        /// </summary>
        /// <param name="defensivePlayer"> The defensive player. </param>
        /// <returns> The generated position. </returns>
        public Position AttackedPositionGenerate(Player defensivePlayer)
        {
            Position attackedPosition = null;
            do
            {
                int generatedRow = this.rnd.Next(0, defensivePlayer.Cells.GetLength(0));
                int generatedColumn = this.rnd.Next(0, defensivePlayer.Cells.GetLength(1));
                if (defensivePlayer.Cells[generatedRow, generatedColumn] == PossibleAttacks.DidNotAttacked)
                {
                    attackedPosition = new Position(generatedRow, generatedColumn);
                }
            }
            while (attackedPosition == null);
            return attackedPosition;
        }

        /// <summary>
        /// Generate all position for the ships.
        /// </summary>
        /// <param name="playerShips"> List of player's ships. </param>
        /// <returns> Changed list of player's ships. </returns>
        private List<Ships> ShipPositionsINIT(List<Ships> playerShips)
        {
            for (int i = 0; i < playerShips.Count; i++)
            {
                do
                {
                    Ships ship = playerShips[i];
                    ship.Positions.Clear();
                    switch (this.rnd.Next(0, 2))
                    {
                        case 0:
                            ship.Positions = ship.PositionListInit(new Position(this.rnd.Next(0, 10), this.rnd.Next(0, 10 - ship.Length)));
                            break;

                        case 1:
                            ship.VerticalImage = true;
                            ship.Positions = ship.PositionListInit(new Position(this.rnd.Next(0, 10 - ship.Length), this.rnd.Next(0, 10)));
                            break;
                    }
                }
                while (this.ShipCollision());
            }

            return playerShips;
        }
    }
}
