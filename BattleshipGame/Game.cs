﻿// <copyright file="Game.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    using System;
    using System.Windows;

    /// <summary>
    /// Game class.
    /// </summary>
    public class Game
    {
        /// <summary>
        /// True if the first player's round coming.
        /// </summary>
        private static bool firstPlayerRound;

        /// <summary>
        /// Stores the game object.
        /// </summary>
        private static Game newGame;

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// The constructor of the Game class.
        /// </summary>
        private Game()
        {
            firstPlayerRound = true;
            this.PlayersINIT();
        }

        /// <summary>
        /// Gets or sets a value indicating whether the first player round starting or not.
        /// </summary>
        public static bool FirstPlayerRound
        {
            get
            {
                return firstPlayerRound;
            }

            set
            {
                firstPlayerRound = value;
            }
        }

        /// <summary>
        /// If the Game object is null, then initialize the Game object.
        /// </summary>
        /// <returns> The Game object. </returns>
        public static Game Get()
        {
            if (newGame == null)
            {
                newGame = new Game();
            }

            return newGame;
        }

        /// <summary>
        /// If one player win, the game is over.
        /// </summary>
        /// <param name="firstPlayer"> The first player. </param>
        /// <param name="secondPlayer"> The second player. </param>
        public void GameOver(Player firstPlayer, Player secondPlayer)
        {
            if (firstPlayer.LiveShips == 0)
            {
                MessageBox.Show("Second Player Wins!");
            }
            else if (secondPlayer.LiveShips == 0)
            {
                MessageBox.Show("First Player Wins!");
            }
            else
            {
                return;
            }

            Environment.Exit(0);
        }

        /// <summary>
        /// Find the selected ship from the player's ship list.
        /// </summary>
        /// <param name="selectedPosition"> The selected position. </param>
        /// <returns> The selected ship. </returns>
        public Ships ShipSelect(Position selectedPosition)
        {
            int i = 0;
            int j = 0;
            Player selectedPlayer;

            selectedPlayer = FirstPlayerRound ? ViewModel.Get().FirstPlayer : ViewModel.Get().SecondPlayer;

            while (i < selectedPlayer.PlayerShips.Count && !(selectedPlayer.PlayerShips[i].Positions[j].X == selectedPosition.X && selectedPlayer.PlayerShips[i].Positions[j].Y == selectedPosition.Y))
            {
                if (j == selectedPlayer.PlayerShips[i].Length - 1)
                {
                    j = 0;
                    i++;
                }
                else
                {
                    j++;
                }
            }

            return (i < selectedPlayer.PlayerShips.Count) ? selectedPlayer.PlayerShips[i] : null;
        }

        /// <summary>
        /// Start of the attack.
        /// </summary>
        /// <param name="attackingPlayer"> The attacking player. </param>
        /// <param name="defensivePlayer"> The defensive player. </param>
        /// <param name="attackedPosition"> The attacked position. </param>
        public void StartAttack(Player attackingPlayer, Player defensivePlayer, Position attackedPosition)
        {
            if (attackingPlayer.Attack(attackedPosition, defensivePlayer))
            {
                if (MainWindow.SelectedGameMode == GameMode.OnePlayer)
                {
                    defensivePlayer.Attack((defensivePlayer as Bot).AttackedPositionGenerate(attackingPlayer), attackingPlayer);
                }
                else
                {
                    FirstPlayerRound = !FirstPlayerRound;
                }

                ViewModel.Get().DrawingChange();
            }
        }

        /// <summary>
        /// Moves the selected image to the selected direction.
        /// </summary>
        /// <param name="selectedMoveability"> The selected moveability. </param>
        /// <param name="selectedShip"> The selected ship. </param>
        public void Moving(MoveAbility selectedMoveability, Ships selectedShip)
        {
            if (selectedShip != null)
            {
                switch (selectedMoveability)
                {
                    case MoveAbility.Up:
                        if (selectedShip.Positions[0].X > 0)
                        {
                            selectedShip.Positions = selectedShip.PositionListInit(new Position(selectedShip.Positions[0].X - 1, selectedShip.Positions[0].Y));
                        }

                        break;
                    case MoveAbility.Down:
                        if ((!selectedShip.VerticalImage && selectedShip.Positions[0].X < 9) || (selectedShip.VerticalImage && selectedShip.Positions[0].X + selectedShip.Length < 10))
                        {
                            selectedShip.Positions = selectedShip.PositionListInit(new Position(selectedShip.Positions[0].X + 1, selectedShip.Positions[0].Y));
                        }

                        break;
                    case MoveAbility.Left:
                        if (selectedShip.Positions[0].Y > 0)
                        {
                            selectedShip.Positions = selectedShip.PositionListInit(new Position(selectedShip.Positions[0].X, selectedShip.Positions[0].Y - 1));
                        }

                        break;
                    case MoveAbility.Right:
                        if ((!selectedShip.VerticalImage && selectedShip.Positions[0].Y + selectedShip.Length < 10) || (selectedShip.VerticalImage && selectedShip.Positions[0].Y < 9))
                        {
                            selectedShip.Positions = selectedShip.PositionListInit(new Position(selectedShip.Positions[0].X, selectedShip.Positions[0].Y + 1));
                        }

                        break;
                    case MoveAbility.Rotate:
                        if (selectedShip.VerticalImage && selectedShip.Positions[0].Y + selectedShip.Length > 10)
                        {
                            selectedShip.Positions = selectedShip.PositionListInit(new Position(selectedShip.Positions[0].X, selectedShip.Positions[0].Y - (9 - selectedShip.Positions[0].Y + selectedShip.Length - 1)));
                        }
                        else if (!selectedShip.VerticalImage && selectedShip.Positions[0].X + selectedShip.Length > 10)
                        {
                            selectedShip.Positions = selectedShip.PositionListInit(new Position(selectedShip.Positions[0].X - (9 - selectedShip.Positions[0].X + selectedShip.Length - 1), selectedShip.Positions[0].Y));
                        }
                        else
                        {
                            selectedShip.Positions = selectedShip.PositionListInit(selectedShip.Positions[0]);
                        }

                        selectedShip.VerticalImage = !selectedShip.VerticalImage;
                        break;
                }

                ViewModel.Get().DrawingChange();
            }
        }

        /// <summary>
        /// End of placement.
        /// </summary>
        public void EndPlacement()
        {
            Player selectedPlayer;
            selectedPlayer = FirstPlayerRound ? ViewModel.Get().FirstPlayer : ViewModel.Get().SecondPlayer;
            if (!selectedPlayer.ShipCollision())
            {
                if (MainWindow.SelectedGameMode == GameMode.OnePlayer)
                {
                    selectedPlayer.InitialMatrix = selectedPlayer.InitialMatrixInit(selectedPlayer.PlayerShips);
                    ViewModel.Get().Preparation = !ViewModel.Get().Preparation;
                }
                else
                {
                    if (!FirstPlayerRound)
                    {
                        ViewModel.Get().Preparation = !ViewModel.Get().Preparation;
                    }

                    selectedPlayer.InitialMatrix = selectedPlayer.InitialMatrixInit(selectedPlayer.PlayerShips);
                    FirstPlayerRound = !FirstPlayerRound;
                }

                ViewModel.Get().DrawingChange();
            }
        }

        /// <summary>
        /// Initialize two player.
        /// </summary>
        private void PlayersINIT()
        {
            ViewModel.Get().FirstPlayer = new HumanPlayer();
            if (MainWindow.SelectedGameMode == GameMode.OnePlayer)
            {
                ViewModel.Get().SecondPlayer = new Bot();
            }
            else
            {
                ViewModel.Get().SecondPlayer = new HumanPlayer();
            }
        }
    }
}
