﻿#pragma checksum "..\..\JatekterWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "1AB72E819204E144A009C746A9BBD2F4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using BattleshipGame;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace BattleshipGame {
    
    
    /// <summary>
    /// JatekterWindow
    /// </summary>
    public partial class JatekterWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\JatekterWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Csatater;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\JatekterWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid IranyitoGrid;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\JatekterWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFel;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\JatekterWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnJobb;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\JatekterWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLe;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\JatekterWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBal;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\JatekterWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnForgatas;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\JatekterWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnElhelyezesVege;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\JatekterWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgElsoJatekos;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\JatekterWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgMasodikJatekosPalya;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/BattleshipGame;component/jatekterwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\JatekterWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Csatater = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.IranyitoGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.btnFel = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\JatekterWindow.xaml"
            this.btnFel.Click += new System.Windows.RoutedEventHandler(this.BtnFel_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnJobb = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\JatekterWindow.xaml"
            this.btnJobb.Click += new System.Windows.RoutedEventHandler(this.BtnJobb_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnLe = ((System.Windows.Controls.Button)(target));
            
            #line 42 "..\..\JatekterWindow.xaml"
            this.btnLe.Click += new System.Windows.RoutedEventHandler(this.BtnLe_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btnBal = ((System.Windows.Controls.Button)(target));
            
            #line 56 "..\..\JatekterWindow.xaml"
            this.btnBal.Click += new System.Windows.RoutedEventHandler(this.BtnBal_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnForgatas = ((System.Windows.Controls.Button)(target));
            
            #line 70 "..\..\JatekterWindow.xaml"
            this.btnForgatas.Click += new System.Windows.RoutedEventHandler(this.BtnForgatas_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnElhelyezesVege = ((System.Windows.Controls.Button)(target));
            
            #line 75 "..\..\JatekterWindow.xaml"
            this.btnElhelyezesVege.Click += new System.Windows.RoutedEventHandler(this.BtnElhelyezesVege_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.imgElsoJatekos = ((System.Windows.Controls.Image)(target));
            return;
            case 10:
            this.imgMasodikJatekosPalya = ((System.Windows.Controls.Image)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

