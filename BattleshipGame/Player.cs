﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    using System.Collections.Generic;

    /// <summary>
    /// Parent class of player.
    /// </summary>
    public abstract class Player
    {
        /// <summary>
        /// The constructor of the Player class.
        /// </summary>
        protected Player()
        {
            this.LiveShips = 5;
            this.InitialMatrix = new char[10, 10];
            this.Cells = new PossibleAttacks[10, 10];
            this.PlayerShips = this.ShipINIT();
        }

        /// <summary>
        /// Gets or sets the number of live ships.
        /// </summary>
        public int LiveShips { get; set; }

        /// <summary>
        /// Gets or sets the list of player's ships.
        /// </summary>
        public List<Ships> PlayerShips { get; set; }

        /// <summary>
        /// Gets or sets a matrix of ship's initials.
        /// </summary>
        public char[,] InitialMatrix { get; set; }

        /// <summary>
        /// Gets or sets a matrix of possible attacks.
        /// </summary>
        public PossibleAttacks[,] Cells { get; set; }

        /// <summary>
        /// This will check if two ships collide with each other.
        /// </summary>
        /// <returns> If the two ships collide, the return value is true, otherwise it is false. </returns>
        public bool ShipCollision()
        {
            for (int i = 0; i < this.PlayerShips.Count - 1; i++)
            {
                for (int j = i + 1; j < this.PlayerShips.Count; j++)
                {
                    if (this.PlayerShips[i].CollisionTester(this.PlayerShips[j].Positions))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Attacking the opponent.
        /// </summary>
        /// <param name="attackedPosition"> The attacked position. </param>
        /// <param name="attackedPlayer"> The attacked player. </param>
        /// <returns> If the position was attacked return true else return false. </returns>
        public bool Attack(Position attackedPosition, Player attackedPlayer)
        {
            if (attackedPlayer.Cells[attackedPosition.X, attackedPosition.Y] == PossibleAttacks.DidNotAttacked)
            {
                if (attackedPlayer.InitialMatrix[attackedPosition.X, attackedPosition.Y] == '\0')
                {
                    attackedPlayer.Cells[attackedPosition.X, attackedPosition.Y] = PossibleAttacks.DidNotHit;
                }
                else
                {
                    int i = 0;
                    while (i < attackedPlayer.PlayerShips.Count && attackedPlayer.PlayerShips[i].Initial != attackedPlayer.InitialMatrix[attackedPosition.X, attackedPosition.Y])
                    {
                        i++;
                    }

                    if (attackedPlayer.PlayerShips[i].Life > 1)
                    {
                        attackedPlayer.PlayerShips[i].Life--;
                        attackedPlayer.Cells[attackedPosition.X, attackedPosition.Y] = PossibleAttacks.Hit;
                    }
                    else
                    {
                        attackedPlayer.PlayerShips[i].Life--;
                        for (int j = 0; j < attackedPlayer.PlayerShips[i].Length; j++)
                        {
                            attackedPlayer.Cells[attackedPlayer.PlayerShips[i].Positions[j].X, attackedPlayer.PlayerShips[i].Positions[j].Y] = PossibleAttacks.HitAndSink;
                        }

                        attackedPlayer.LiveShips--;
                    }
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Initialize the InitialMatrix.
        /// </summary>
        /// <param name="playerShips"> List of player's ships. </param>
        /// <returns> Initialized InitialMatrix. </returns>
        public char[,] InitialMatrixInit(List<Ships> playerShips)
        {
            char[,] initialMatrix = new char[10, 10];
            for (int i = 0; i < playerShips.Count; i++)
            {
                for (int j = 0; j < playerShips[i].Positions.Count; j++)
                {
                    initialMatrix[playerShips[i].Positions[j].X, playerShips[i].Positions[j].Y] = playerShips[i].Initial;
                }
            }

            return initialMatrix;
        }

        /// <summary>
        /// Initialize the PlayersShips.
        /// </summary>
        /// <returns> Initialized PlayersShips. </returns>
        private List<Ships> ShipINIT()
        {
            List<Ships> playerShips = new List<Ships>();
            playerShips.Add(new DestroyerShip());
            playerShips.Add(new CorvetteShip());
            playerShips.Add(new SubmarineShip());
            playerShips.Add(new BattleShip());
            playerShips.Add(new AircraftCarrierShip());

            return playerShips;
        }
    }
}
