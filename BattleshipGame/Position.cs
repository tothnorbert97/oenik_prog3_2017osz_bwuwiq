﻿// <copyright file="Position.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    /// <summary>
    /// Position class.
    /// </summary>
    public class Position
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Position"/> class.
        /// The constructor of the Position class.
        /// </summary>
        /// <param name="x"> The stored row. </param>
        /// <param name="y"> The stored column. </param>
        public Position(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Gets or sets the stored row.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Gets or sets the stored column.
        /// </summary>
        public int Y { get; set; }
    }
}
