﻿// <copyright file="GameMode.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    /// <summary>
    /// Enum with game modes.
    /// </summary>
    public enum GameMode
    {
        /// <summary>
        /// One player game mode.
        /// </summary>
        OnePlayer,

        /// <summary>
        /// Two player game mode.
        /// </summary>
        TwoPlayer
    }
}
