﻿// <copyright file="BattleShip.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    using System;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// BattleShip type Ships subclass.
    /// </summary>
    public class BattleShip : Ships
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BattleShip"/> class.
        /// The constructor of the BattleShip class.
        /// </summary>
        public BattleShip()
            : base('B', 4)
        {
            this.StandardImage = new BitmapImage(new Uri("../../images/StandardBattleship.png", UriKind.Relative));
            this.RotatedImage = new BitmapImage(new Uri("../../images/RotatedBattleship.png", UriKind.Relative));
            this.Positions = this.PositionListInit(new Position(1, 0));
        }
    }
}
