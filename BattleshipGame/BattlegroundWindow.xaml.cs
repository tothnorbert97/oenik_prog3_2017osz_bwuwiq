﻿// <copyright file="BattlegroundWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// BattlegroundWindow type Window subclass.
    /// </summary>
    public partial class BattlegroundWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BattlegroundWindow"/> class.
        /// The constructor of the BattlegroundWindow class.
        /// </summary>
        public BattlegroundWindow()
        {
            this.InitializeComponent();
            this.DataContext = ViewModel.Get();
            Game.Get();
            this.imgFirstPlayer.MouseLeftButtonDown += this.Img_MouseLeftButtonDown;
            this.imgSecondPlayer.MouseLeftButtonDown += this.Img_MouseLeftButtonDown;
        }

        private void Img_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Image playerImage = (Image)sender;
            Point selectedPoint = e.GetPosition(playerImage);
            Position selectedPosition = new Position((int)(selectedPoint.Y / 40), (int)(selectedPoint.X / 40));

            if (ViewModel.Get().Preparation)
            {
                ViewModel.Get().SelectedShip = Game.Get().ShipSelect(selectedPosition);
            }
            else
            {
                if (Game.FirstPlayerRound && playerImage == this.imgSecondPlayer)
                {
                    Game.Get().StartAttack(ViewModel.Get().FirstPlayer, ViewModel.Get().SecondPlayer, selectedPosition);
                }
                else if (!Game.FirstPlayerRound && playerImage == this.imgFirstPlayer)
                {
                    Game.Get().StartAttack(ViewModel.Get().SecondPlayer, ViewModel.Get().FirstPlayer, selectedPosition);
                }
            }

            Game.Get().GameOver(ViewModel.Get().FirstPlayer, ViewModel.Get().SecondPlayer);
        }

        private void BtnUp_Click(object sender, RoutedEventArgs e)
        {
            Game.Get().Moving(MoveAbility.Up, ViewModel.Get().SelectedShip);
        }

        private void BtnDown_Click(object sender, RoutedEventArgs e)
        {
            Game.Get().Moving(MoveAbility.Down, ViewModel.Get().SelectedShip);
        }

        private void BtnLeft_Click(object sender, RoutedEventArgs e)
        {
            Game.Get().Moving(MoveAbility.Left, ViewModel.Get().SelectedShip);
        }

        private void BtnRight_Click(object sender, RoutedEventArgs e)
        {
            Game.Get().Moving(MoveAbility.Right, ViewModel.Get().SelectedShip);
        }

        private void BtnRotate_Click(object sender, RoutedEventArgs e)
        {
            Game.Get().Moving(MoveAbility.Rotate, ViewModel.Get().SelectedShip);
        }

        private void BtnEndPlacement_Click(object sender, RoutedEventArgs e)
        {
            Game.Get().EndPlacement();
            if (!ViewModel.Get().Preparation)
            {
                this.gridBattleground.Children.Remove(this.gridControlPanel);
            }
        }
    }
}
