﻿// <copyright file="MoveAbility.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    /// <summary>
    /// Enum with moveabilitys.
    /// </summary>
    public enum MoveAbility
    {
        /// <summary>
        /// Moving the image up.
        /// </summary>
        Up,

        /// <summary>
        /// Moving the image down.
        /// </summary>
        Down,

        /// <summary>
        /// Moving the image left.
        /// </summary>
        Left,

        /// <summary>
        /// Moving the image right.
        /// </summary>
        Right,

        /// <summary>
        /// Rotate the image.
        /// </summary>
        Rotate
    }
}
