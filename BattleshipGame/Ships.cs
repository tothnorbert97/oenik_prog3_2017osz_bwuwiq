﻿// <copyright file="Ships.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    using System.Collections.Generic;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Parent class of ships.
    /// </summary>
    public abstract class Ships
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Ships"/> class.
        /// The constructor of the DestroyerShip class.
        /// </summary>
        /// <param name="initial"> The initial of the player's ship. </param>
        /// <param name="life"> The life of the player's ship. </param>
        protected Ships(char initial, int life)
        {
            this.Initial = initial;
            this.Length = life;
            this.Life = life;
            this.Positions = new List<Position>();
        }

        /// <summary>
        /// Gets or sets a value indicating whether the direction of the ship's location.
        /// </summary>
        public bool VerticalImage { get; set; }

        /// <summary>
        /// Gets or sets the initial of the player's ship.
        /// </summary>
        public char Initial { get; set; }

        /// <summary>
        /// Gets or sets the length of the player's ship.
        /// </summary>
        public int Length { get; set; }

        /// <summary>
        /// Gets or sets the life of the player's ship.
        /// </summary>
        public int Life { get; set; }

        /// <summary>
        /// Gets or sets the standard image of the player's ship.
        /// </summary>
        public BitmapImage StandardImage { get; set; }

        /// <summary>
        /// Gets or sets the rotated image of the player's ship.
        /// </summary>
        public BitmapImage RotatedImage { get; set; }

        /// <summary>
        /// Gets or sets the positions of the player's ship.
        /// </summary>
        public List<Position> Positions { get; set; }

        /// <summary>
        /// This will check if two ship's position collide with each other.
        /// </summary>
        /// <param name="otherShipPositions"> The ship that is investigating the position collision. </param>
        /// <returns> If the two ship's position collide, the return value is true, otherwise it is false. </returns>
        public bool CollisionTester(List<Position> otherShipPositions)
        {
            foreach (Position positionOne in this.Positions)
            {
                foreach (Position positionTwo in otherShipPositions)
                {
                    if (positionOne.X == positionTwo.X && positionOne.Y == positionTwo.Y)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Initialize the Positions.
        /// </summary>
        /// <param name="startingPosition"> The ship's first position. </param>
        /// <returns> Initialized Positions. </returns>
        public List<Position> PositionListInit(Position startingPosition)
        {
            List<Position> newPositionsList = new List<Position>();
            if (!this.VerticalImage)
            {
                for (int i = startingPosition.Y; i < startingPosition.Y + this.Length; i++)
                {
                    newPositionsList.Add(new Position(startingPosition.X, startingPosition.Y + (i - startingPosition.Y)));
                }
            }
            else
            {
                for (int i = startingPosition.X; i < startingPosition.X + this.Length; i++)
                {
                    newPositionsList.Add(new Position(startingPosition.X + (i - startingPosition.X), startingPosition.Y));
                }
            }

            return newPositionsList;
        }
    }
}
