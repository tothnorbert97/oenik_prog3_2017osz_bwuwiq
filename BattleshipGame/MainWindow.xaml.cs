﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// MainWindow type Window subclass.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Selected GameMode.
        /// </summary>
        private static GameMode selectedGameMode;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// The constructor of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the selected GameMode.
        /// </summary>
        public static GameMode SelectedGameMode
        {
            get
            {
                return selectedGameMode;
            }

            set
            {
                selectedGameMode = value;
            }
        }

        private void BtnGameMode_Click(object sender, RoutedEventArgs e)
        {
            SelectedGameMode = (Button)sender == this.btnPlayerVSBot ? GameMode.OnePlayer : GameMode.TwoPlayer;
            BattlegroundWindow newBattlegroundWindow = new BattlegroundWindow();
            newBattlegroundWindow.ShowDialog();
        }
    }
}
