﻿// <copyright file="SubmarineShip.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    using System;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// SubmarineShip type Ships subclass.
    /// </summary>
    public class SubmarineShip : Ships
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SubmarineShip"/> class.
        /// The constructor of the SubmarineShip class.
        /// </summary>
        public SubmarineShip()
            : base('S', 3)
        {
            this.StandardImage = new BitmapImage(new Uri("../../images/StandardSubmarine.png", UriKind.Relative));
            this.RotatedImage = new BitmapImage(new Uri("../../images/RotatedSubmarine.png", UriKind.Relative));
            this.Positions = this.PositionListInit(new Position(2, 0));
        }
    }
}
