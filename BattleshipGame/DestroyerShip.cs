﻿// <copyright file="DestroyerShip.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BattleshipGame
{
    using System;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// DestroyerShip type Ships subclass.
    /// </summary>
    public class DestroyerShip : Ships
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DestroyerShip"/> class.
        /// The constructor of the DestroyerShip class.
        /// </summary>
        public DestroyerShip()
            : base('D', 2)
        {
            this.StandardImage = new BitmapImage(new Uri("../../images/StandardDestroyer.png", UriKind.Relative));
            this.RotatedImage = new BitmapImage(new Uri("../../images/RotatedDestroyer.png", UriKind.Relative));
            this.Positions = this.PositionListInit(new Position(4, 0));
        }
    }
}
